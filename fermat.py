import math


def fermat(n: int) -> list:
    if n < 2:
        raise ValueError('n must be greater then 1')

    x = math.ceil(math.sqrt(n))
    y = 0
    r = math.ceil(math.pow(x, 2) - math.pow(y, 2) - n)

    while r != 0:
        if r > 0:
            y += 1
        else:
            x += 1
        r = math.ceil(math.pow(x, 2) - math.pow(y, 2) - n)

    return [x + y, x - y]
