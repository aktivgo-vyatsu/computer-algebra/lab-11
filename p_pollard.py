import random
from math import gcd


def f(x: int, n: int) -> int:
    return int(x * x + 1) % n


def p_pollard(n: int, iterations: int) -> list:
    if n < 9:
        raise ValueError('n must be greater then 8')

    X = [random.randint(2, 10)]
    for i in range(1, iterations + 2):
        X.append(f(X[i - 1], n))

    for i in range(iterations):
        for k in range(1, i + 2):
            for j in range(i + 1):
                curr_gcd = gcd(abs(X[j] - X[k]), n)
                if 1 < curr_gcd < n:
                    return [curr_gcd, n // curr_gcd]

    return [1, n]
