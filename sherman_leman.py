from math import isqrt, sqrt, gcd


def sherman_leman(n: int) -> list:
    if n < 9:
        raise ValueError('n must be greater then 8')

    for a in range(2, int(pow(n, 1 / 3))):
        if n % a == 0:
            return [a, n // a]

    for k in range(1, int(pow(n, 1 / 3))):
        for d in range(int(pow(n, 1 / 6) / (4 * sqrt(k))) + 2):
            sqr = pow(int(sqrt(4 * k * n)) + d, 2) - 4 * k * n
            if sqr >= 1 and sqr == isqrt(sqr) ** 2:
                A = int(sqrt(4 * k * n)) + d
                B = isqrt(A * A - 4 * k * n)
                A_plus_B_GCD = gcd(A + B, n)
                A_minus_B_GCD = gcd(A - B, n)
                if 1 < A_plus_B_GCD < n and 1 < A_minus_B_GCD < n:
                    return [A_minus_B_GCD, n // A_minus_B_GCD]

    return [1, n]
